package com.fbstudio.exchangeapp;

import com.fbstudio.exchangeapp.data.RequestManager;

import org.junit.Test;

import static org.junit.Assert.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


/**
 * Created by fbejarano on 28/04/2018.
 */

public class TestRequestManager {

    @Test
    public void testFetchCurrencies(){
        String response= RequestManager.fetchCurrenciesList();

        assertNotNull(response);
        assertTrue(response.length() > 50);


    }

    @Test
    public void testFetchExchangeRate(){
        String response= RequestManager.fetchExchangeRate("USD","ARS");

        assertNotNull(response);

        assertTrue(response.length() >10);
    }

    @Test
    public void testFetchExchangeRate2(){
        String response= RequestManager.fetchExchangeRate("ARS","USD");

        assertNotNull(response);

        assertTrue(response.length() >10);
    }

    /*
    Test an invalid request to the public api.
    To test this, it is necessary to call directly the private method fetchExchangeRate with reflection.
     */
    @Test
    public void testFetchInvalid() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Class clazz=RequestManager.class;
        Method method = clazz.getDeclaredMethod("makeRequest",String.class);
        method.setAccessible(true);
        Object r = method.invoke(clazz,"https://free.currencyconverterapi.com/api/v5/currenciesXYZ");

        assertNull(r);
    }
}
