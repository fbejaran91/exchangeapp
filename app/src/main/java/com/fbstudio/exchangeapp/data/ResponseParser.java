package com.fbstudio.exchangeapp.data;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by fbejarano on 28/04/2018.
 */

public class ResponseParser {

    private static final String TAG = "ResponseParser";


    /*
    Parses the list of currencies coming from the public api

    @param: json representation of the list of currencies
    @return: list of currency codes available
     */
    public static List<String> getCurrenciesList(String responseJson) {
        List<String> ans = new ArrayList<String>();
        JSONObject obj;

        if(responseJson ==null){
            Log.e(TAG,"response Json is invalid");
            return null;
        }

        try {
            obj = new JSONObject(responseJson);
        } catch (JSONException e) {
            Log.e(TAG, "error parsing json string", e);
            return null;
        }

        JSONObject resultsJson;
        try {
            resultsJson = obj.getJSONObject("results");

            Iterator<String> currencyKeyIterator = resultsJson.keys();

            String currencyID;
            JSONObject currencyObj;

            String currencyKey;
            while (currencyKeyIterator.hasNext()) {
                currencyKey = currencyKeyIterator.next();

                currencyObj = resultsJson.getJSONObject(currencyKey);
                currencyID = currencyObj.getString("id");

                ans.add(currencyID);
            }
        } catch (JSONException e) {
            Log.e(TAG, "Error parsing list of currencies.");
            return null;
        }

        return ans;

    }


    /*
    Parses the exchange rates coming from the public api.

    @param: json representation of the exchange rates
    @return: map that contains as key <ORIGIN CURRENCY>_<DESTINATION CURRENCY>
    and as value the exchage rate for this conversion
     */
    public static Map<String, Double> getExchangeRates(String str) {
        Map<String, Double> ans = new HashMap<String, Double>();


        JSONObject obj;
        try {
            obj = new JSONObject(str);
        } catch (JSONException e) {
            Log.e(TAG, "error parsing json string", e);
            return null;
        }

        Iterator<String> rates = obj.keys();
        String rateId;
        Double rateValue;

        try {
            while (rates.hasNext()) {

                rateId = rates.next();

                rateValue = obj.getDouble(rateId);
                ans.put(rateId,rateValue);

            }
        } catch (JSONException e) {
            Log.e(TAG, "Error parsing list of rates");
        }

        return ans;
    }
}
