package com.fbstudio.exchangeapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.util.Log;

import com.fbstudio.exchangeapp.data.DataManager;

import java.util.List;

/**
 * Created by fbejarano on 29/04/2018.
 */

public class InitAsyncTask extends AsyncTask<Object, Void, Boolean> {

    private static final String TAG="InitAsycTask";
    private ProgressDialog progressDialog;
    private MainActivity mainActivity;
    private List<String> currencies;


    public InitAsyncTask(ProgressDialog dialog,MainActivity mainActivity){
        this.progressDialog =dialog;
        this.mainActivity=mainActivity;
        currencies=null;
    }

    @Override
    protected Boolean doInBackground(Object... objs) {
        DataManager dataManager=DataManager.getInstance();
        currencies=dataManager.getCurrenciesList();

        Log.d(TAG,"doInBackground start");
        if(currencies ==null){
            Log.d(TAG,"Currencies list is null");
            return new Boolean("false");
        }
        if(currencies.size()==0) {
            Log.d(TAG,"Currencies list is empty");
            return new Boolean("false");

        }
        Log.d(TAG,"doInBackground end");

        return new Boolean("true");
    }

    @Override
    protected void onPostExecute(Boolean result) {
        Log.d(TAG,"onPostExecute start");
        if (result && currencies!=null) {
            mainActivity.setCurrencies(currencies);
            progressDialog.cancel();
        }else{
            //Display a message to notify a connection problem
            Log.d(TAG,"currencies is not initialized.Connection problem.");
            AlertDialog alertDialog=createConnectionErrorDialog();

            progressDialog.cancel();
            alertDialog.show();

        }

        Log.d(TAG,"onPostExecute end");
    }

    /*
    Returns an AlertDialog that is used to inform that a connection problem occurred.
     */
    private AlertDialog createConnectionErrorDialog(){

        AlertDialog ans;
        DialogInterface.OnClickListener listener= new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mainActivity.init();
            }
        };
        ans=ConnectionDialogBuilder.buildConnectionErrorDialog(mainActivity,listener);


        return ans;
    }

}
