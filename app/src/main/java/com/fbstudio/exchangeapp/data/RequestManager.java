package com.fbstudio.exchangeapp.data;

import android.util.Log;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

/**
 * Created by fbejarano on 28/04/2018.
 */

public class RequestManager {

    private static final String TAG = "RequestManager";
    private static final String CURRENCIES_REQUEST = "https://free.currencyconverterapi.com/api/v5/currencies";
    private static final String EXCHANGE_RATE_REQUEST = "https://free.currencyconverterapi.com/api/v5/convert?q=%s_%s,%s_%s&compact=ultra";
    private static final int CONNECTION_TIMEOUT=5000;
    /*
   Gets the exchange rate BaseCurrency->DestinationCurrency and DestinationCurrency->BaseCurrency from the public api.
    */
    public static String fetchExchangeRate(String baseCurrency, String destinationCurrency) {
        String url = String.format(EXCHANGE_RATE_REQUEST
                , baseCurrency
                , destinationCurrency
                , destinationCurrency
                , baseCurrency);


        String response = makeRequest(url);


        return response;


    }

    public static String fetchCurrenciesList() {

        return makeRequest(CURRENCIES_REQUEST);
    }

    /*
    Makes a http request to the specified url
     */
    private static String makeRequest(String urlStr) {

        URL url = null;
        HttpURLConnection con = null;
        BufferedReader in = null;
        StringBuffer responseBuffer =null;

        Log.d(TAG,"making a request to url:"+urlStr);
        try {
            url = new URL(urlStr);
        } catch (MalformedURLException e) {
            Log.e(TAG, "Invalid url:" + urlStr);
            return null;
        }
        try {
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Content-Type", "application/json");

            con.setConnectTimeout(CONNECTION_TIMEOUT);
            int responceStatus = con.getResponseCode();

            if(responceStatus >= 400){
                Log.e(TAG,"Request failed with code: "+responceStatus);
                return null;
            }
            in = new BufferedReader(
                    new InputStreamReader(con.getInputStream())
            );

            String responseLine;
            responseBuffer=new StringBuffer();

            while((responseLine = in.readLine()) !=null){
                responseBuffer.append(responseLine);

            }

            in.close();
            con.disconnect();




        } catch (Exception e) {
            Log.e(TAG, "Connection error while making request to: "+urlStr);
        }finally {
            if(in !=null) {
                try {
                    in.close();
                } catch (IOException e) {

                }
            }

            if(con !=null) {
                con.disconnect();
            }
        }

        if(responseBuffer==null)
            return null;

        return responseBuffer.toString();
    }
}
