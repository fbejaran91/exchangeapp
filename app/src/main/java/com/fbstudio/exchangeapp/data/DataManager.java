package com.fbstudio.exchangeapp.data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by fbejarano on 28/04/2018.
 */

public class DataManager {


   // private List<String> currenciesList;
   // private Map<String, Double> rates;

    //Singleton pattern
    private static DataManager instance;
    private static Object lock=new Object();
    public static Object List;


    private DataManager(){

        //rates=new HashMap<String,Double>();

    }


    public static DataManager getInstance(){

        synchronized (lock){

            if(instance == null){

                instance=new DataManager();

            }
            return instance;
        }
    }


    public Double getExchangeRate(String baseCurrency, String destinationCurrency){
        String key=String.format("%s_%s",baseCurrency,destinationCurrency);

       // if(!rates.containsKey(key)){

        String requestResultStr=RequestManager.fetchExchangeRate(baseCurrency,destinationCurrency);
        if(requestResultStr==null)
            return null;
        Map<String, Double> requestResult= ResponseParser.getExchangeRates(requestResultStr);


        //}

        if(requestResult ==null) {
            return null;
        }
        return requestResult.get(key);

    }

    public List<String> getCurrenciesList(){
        String requestResult=RequestManager.fetchCurrenciesList();
        if(requestResult==null)
            return null;

        //Sort currecies using alphabetical order.
        List<String> currencies=ResponseParser.getCurrenciesList(requestResult);
        if(currencies==null)
            return null;

        Collections.sort(currencies);



        return currencies;
    }




}
