package com.fbstudio.exchangeapp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.util.Log;
import android.util.Pair;
import android.widget.Spinner;
import android.widget.TextView;

import com.fbstudio.exchangeapp.data.DataManager;

/**
 * Created by fbejarano on 30/04/2018.
 */

public class ConversionAsyncTask extends AsyncTask<String, Void, Pair<ConversionAsyncTask.STATUS,Double>> {

    private static final String TAG = "ConversionAsyncTask";
    private DataManager dataManager;
    private Spinner baseSpinner;
    private Spinner destSpinner;
    private TextView destinationAmountText;
    private Context context;

    private ProgressDialog progressDialog;

    private String baseAmountStr;

    public enum STATUS{OK, CONNECTION_ERR,PARSING_ERR};

    public ConversionAsyncTask(Spinner baseSpinner, Spinner destSpinner, TextView destinationAmountText, Context context){
        dataManager=DataManager.getInstance();

        this.baseSpinner=baseSpinner;
        this.destSpinner=destSpinner;
        this.destinationAmountText=destinationAmountText;
        this.context=context;
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(context.getResources().getString(R.string.loading_rates));
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        baseAmountStr=null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Log.d(TAG,"onPreExecute showing progress dialog");
        progressDialog.show();

    }

    @Override
    protected Pair<STATUS,Double> doInBackground(String... amountStrArray) {
        double baseAmount;

        Log.d(TAG,"doInBackground start");
        if(amountStrArray==null || ! (amountStrArray.length>0)){
            Log.e(TAG,"Invalid arguments");
            return new Pair(STATUS.PARSING_ERR, null);
        }
        baseAmountStr=amountStrArray[0];
        try{
            baseAmount=Double.parseDouble(baseAmountStr);
        }catch( NumberFormatException ex){
            //Invalid number introduced
            Log.d(TAG,"invalid number introduced");
            return new Pair(STATUS.PARSING_ERR, null);

        }

        String baseCurrency=baseSpinner.getItemAtPosition(baseSpinner.getSelectedItemPosition()).toString();
        String destCurrency=destSpinner.getItemAtPosition(destSpinner.getSelectedItemPosition()).toString();
        Double rate=dataManager.getExchangeRate(baseCurrency,destCurrency);

        Log.d(TAG,"Conversion rate:"+rate);
        if(rate==null)
            return new Pair(STATUS.CONNECTION_ERR, null);

        double result=baseAmount*rate;

        Log.d(TAG,"Conversion result:"+result);



        Log.d(TAG,"doInBackground start");
        return new Pair(STATUS.OK, result);
    }

    @Override
    protected void onPostExecute(Pair<STATUS,Double> pair) {


        super.onPostExecute(pair);

       STATUS resultStatus = pair.first;
        Log.d(TAG,"onPostExecute cancelling progress dialog");
        progressDialog.cancel();


        switch(resultStatus){
            case OK:{
                Double result = pair.second;
                destinationAmountText.setText(Double.toString(result));
                break;
            }
            case PARSING_ERR:{
                Log.d(TAG,"Invalid number introduced");
                destinationAmountText.setText( context.getString(R.string.invalid_number_hint));
                break;
            }
            case CONNECTION_ERR:{
                Log.e(TAG,"Connection problem.");
                AlertDialog alertDialog=createConnectionErrorDialog();
                alertDialog.show();
                break;
            }
        }



    }

    private AlertDialog createConnectionErrorDialog(){


        DialogInterface.OnClickListener onClickListener= new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ConversionAsyncTask conversionAsyncTask=new ConversionAsyncTask(baseSpinner, destSpinner,destinationAmountText,context);
                conversionAsyncTask.execute(baseAmountStr);
            }
        };



        return ConnectionDialogBuilder.buildConnectionErrorDialog(context,onClickListener);
    }
}
