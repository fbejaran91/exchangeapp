package com.fbstudio.exchangeapp;

import com.fbstudio.exchangeapp.data.ResponseParser;

import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Created by fbejarano on 28/04/2018.
 */

public class TestParser {
    private static final String TAG = "TestParser";

    private String allCurrenciesJson;
    private String exchangeRatesJson;

    @Before
    public void init() throws IOException {
        FileInputStream inputStream = new FileInputStream("app/src/main/assets/list_currencies_example.json");

        allCurrenciesJson = IOUtils.toString(inputStream);

        FileInputStream inputStream2 = new FileInputStream("app/src/main/assets/list_exchange_rates_example.json");

        exchangeRatesJson = IOUtils.toString(inputStream2);

    }

    @Test
    public void testGetCurrencies() {

        List<String> ans = ResponseParser.getCurrenciesList(allCurrenciesJson);


        assertNotNull(ans);

        assertEquals(4, ans.size());

        assertTrue(ans.contains("EUR"));
        assertFalse(ans.contains("ARS"));
        assertTrue(ans.contains("BBD"));


    }

    @Test
    public void testGetExchangeRate() {
        Map<String, Double> exchangeRates=null;

        exchangeRates= ResponseParser.getExchangeRates(exchangeRatesJson);
        assertNotNull(exchangeRates);

        assertEquals(2,exchangeRates.size());
        assertTrue(exchangeRates.containsKey("ARS_USD"));
        assertTrue(exchangeRates.containsKey("USD_ARS"));


    }
}
