package com.fbstudio.exchangeapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

/**
 * Created by fbejarano on 30/04/2018.
 */

public class ConnectionDialogBuilder {


   public static AlertDialog buildConnectionErrorDialog(Context context, DialogInterface.OnClickListener onClickListener){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setTitle(context.getString(R.string.connection_problem_title))
                .setMessage(context.getString(R.string.connection_problem_message))
                .setPositiveButton(context.getString(R.string.retry_msg)
                        ,onClickListener);


        AlertDialog ans=builder.create();

        ans.setCancelable(false);
        return ans;
    }
}
