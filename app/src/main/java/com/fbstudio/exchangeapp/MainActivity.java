package com.fbstudio.exchangeapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String TAG ="MainActivity" ;
    private ProgressDialog progressDialog;
    private EditText editText;
    private TextView destinationAmountText;

    private Context context;

    private Spinner baseSpinner;
    private Spinner destSpinner;

    private AsyncTask<String, Void, Pair<ConversionAsyncTask.STATUS,Double>> conversionAsyncTask;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        progressDialog = new ProgressDialog(MainActivity.this);

        progressDialog.setMessage(getResources().getString(R.string.loading_currencies));
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();

        setContentView(R.layout.activity_main);

        editText=(EditText)findViewById(R.id.baseCurrencyText);

        baseSpinner=findViewById(R.id.baseCurrencyButton);
        destSpinner =(Spinner)findViewById(R.id.destinationCurrencyButton);

        context=this;

        init();
        destinationAmountText=findViewById(R.id.destinationText);


    }

    protected void init(){
        AsyncTask initAsyncTask=new InitAsyncTask(progressDialog,this);

        initAsyncTask.execute();
        configureReverseButton();

    }

    protected void setCurrencies(final List<String> currencies){



        for(Spinner spinner: new Spinner[]{baseSpinner,destSpinner}) {
            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(context
                    , android.R.layout.simple_spinner_item
                    , currencies);

            spinner.setAdapter(spinnerArrayAdapter);

        }



        setEditTextListener();
        setSpinnerListeners();
    }

    private void setEditTextListener(){
        editText.addTextChangedListener(new TextWatcher() {
            private  String TAG="TextWatcher";
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
               String amountStr=s.toString();
               Log.d(TAG,"after text changed:"+ amountStr);
               runConversion(amountStr);
            }
        });


    }



    private void setSpinnerListeners(){
        AdapterView.OnItemSelectedListener listener=new AdapterView.OnItemSelectedListener(){

            private static final String TAG="Spinner listener";
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                Log.d(TAG,"selected item:"+position);
                runConversion();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        };

        baseSpinner.setOnItemSelectedListener(listener);
        destSpinner.setOnItemSelectedListener(listener);
    }


    /*Does the conversion of currency asynchronously.
    * It receives the amount to convert from the edit text field
    * */
    private void runConversion(){

        String amountStr=null;
        amountStr=editText.getText().toString();

        runConversion(amountStr);

    }
    /*Does the conversion of currency asynchronously */
    private void runConversion(String amountStr){
        if(amountStr==null) {
            Log.e(TAG, "received null string as amount to convert.");
            return;
        }
        conversionAsyncTask=new ConversionAsyncTask(baseSpinner, destSpinner,destinationAmountText,context);
        conversionAsyncTask.execute(amountStr);
    }


    private void configureReverseButton(){
        final Button button = findViewById(R.id.reverseButton);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int originalBaseIndex, originalDestinationIndex;

                originalBaseIndex=baseSpinner.getSelectedItemPosition();
                originalDestinationIndex=destSpinner.getSelectedItemPosition();

                baseSpinner.setSelection(originalDestinationIndex,true);
                destSpinner.setSelection(originalBaseIndex,true);
            }
        });
    }

}
